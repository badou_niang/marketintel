from django.urls import path, include
from rest_framework import routers
from marketinteldemo.rest_api import views
from django.conf.urls import url, include


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)

urlpatterns = [
    path('', include(router.urls)),
    # urls(r'^', include('router.urls')),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
