from django.urls import path
from education import views 

urlpatterns = [
    path('niveau_etude', views.Niveau_etude_list),
    path('niveau_etude/<int:pk>', views.Niveau_etude_detail),

	path('admission', views.admission_list),
    path('admission/<str:region>', views.Taux_brut_admission_detail),

	path('scolarisation', views.scolarisation_list),
    path('scolarisation/<str:region>', views.Taux_brut_scolarisation_detail),

    path('universite', views.Universite_list),
    path('universite/<str:nom>', views.Taux_brut_admission_detail),

    path('formation', views.Formation_list),
    path('formation/<str:nom>', views.Formation_detail),

    path('domaine_formation', views.Domaine_formation_list),
    path('domaine_formation/<str:nom>', views.Domaine_formation_detail),

	path('ecole', views.Ecole_list),
    path('ecole/<str:nom>', views.Ecole_detail),

	path('examen', views.Examen_list),
    path('examen/<str:type_exam>', views.Examen_detail),


    # path('api/niveau_etude/published', views.tutorial_list_published)
]