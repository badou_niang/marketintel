# Generated by Django 2.1.7 on 2020-08-10 17:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('education', '0011_auto_20200810_1649'),
    ]

    operations = [
        migrations.RenameField(
            model_name='examen',
            old_name='nbr_candidat_inscrit_pour_examen',
            new_name='nbr_candidat',
        ),
        migrations.RenameField(
            model_name='examen',
            old_name='nbr_presente',
            new_name='nbr_present',
        ),
        migrations.AlterField(
            model_name='examen',
            name='type_exam',
            field=models.CharField(default='', max_length=10),
        ),
    ]
