from rest_framework import serializers 
from education.models import *
 
 
class Niveau_etudeSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Niveau_etude
        fields = ('id',
                  'nom')


class Taux_brut_admissionSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Taux_brut_admission
        fields = ('id',
        		      'region',
                  'garcon',
                  'fille',
                  'total',
                  'annee',
                  'commentaire',
                  'niveau_etude')



class Taux_brut_scolarisationSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Taux_brut_scolarisation
        fields = '__all__'


class UniversiteSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Universite
        fields = '__all__'


class FormationSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Formation
        fields = '__all__'

class Domaine_formationSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Domaine_formation
        fields = '__all__'

class EcoleSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Ecole
        fields = '__all__'

class ExamenSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Examen
        fields = '__all__'

