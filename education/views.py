from django.shortcuts import render

# Create your views here.

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 
# from education.models import Niveau_etude, Taux_brut_admission, Taux_brut_scolarisation, Universite, Formation, Domaine_formation, Ecole, Examen

from education.models import *

# from education.serializers import Niveau_etudeSerializer, Taux_brut_admissionSerializer, Taux_brut_scolarisationSerializer, UniversiteSerializer, FormationSerializer, Domaine_formationSerializer, EcoleSerializer, ExamenSerializer

from education.serializers import *

from rest_framework.decorators import api_view

""" 
GET list of tutorials, POST a new tutorial, DELETE all tutorials
"""
"""Universite List"""
@api_view(['GET', 'POST', 'DELETE'])
def Universite_list(request):
	if request.method == 'GET':
		universite = Universite.objects.all()
		nom = request.GET.get('nom', None)
		if nom is not None:
			universite = Universite.filter(nom__icontains=nom)
			
		Universite_Serializer = UniversiteSerializer(universite, many=True)
		return JsonResponse(Universite_Serializer.data, safe=False)

	elif request.method == 'POST':
		univ_data = JSONParser().parse(request)
		Universite_Serializer = UniversiteSerializer(data=univ_data)
		if Universite_Serializer.is_valid():
			Universite_Serializer.save()
			return JsonResponse(Universite_Serializer.data, status=status.HTTP_201_CREATED) 
		return JsonResponse(Universite_Serializer.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		count = Universite.objects.all().delete()
		return JsonResponse({'message': '{} Universites were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)


"""Formation List"""
@api_view(['GET', 'POST', 'DELETE'])
def Formation_list(request):
    if request.method == 'GET':
        formation = Formation.objects.all()
        nom = request.GET.get('nom', None)
        if nom is not None:
            formation = Formation.filter(nom__icontains=nom)
            
        Formation_Serializer = FormationSerializer(formation, many=True)
        return JsonResponse(Formation_Serializer.data, safe=False)

    elif request.method == 'POST':
        formation_data = JSONParser().parse(request)
        Formation_Serializer = FormationSerializer(data=formation_data)
        if Formation_Serializer.is_valid():
            Formation_Serializer.save()
            return JsonResponse(Formation_Serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(Formation_Serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Formation.objects.all().delete()
        return JsonResponse({'message': '{} Formations were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)



"""Domaine_formation List"""
@api_view(['GET', 'POST', 'DELETE'])
def Domaine_formation_list(request):
    if request.method == 'GET':
        domaine_formation = Domaine_formation.objects.all()
        nom = request.GET.get('nom', None)
        if nom is not None:
            domaine_formation = Domaine_formation.filter(nom__icontains=nom)
            
        Formation_Serializer = Domaine_formationSerializer(domaine_formation, many=True)
        ###
        return JsonResponse(Formation_Serializer.data, safe=False)

    elif request.method == 'POST':
        formation_data = JSONParser().parse(request)
        Formation_Serializer = Domaine_formationSerializer(data=formation_data)
        if Formation_Serializer.is_valid():
            Formation_Serializer.save()
            return JsonResponse(Formation_Serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(Formation_Serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Domaine_formation.objects.all().delete()
        return JsonResponse({'message': '{} domaine were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)



"""Ecole List"""
@api_view(['GET', 'POST', 'DELETE'])
def Ecole_list(request):
    if request.method == 'GET':
        ecole = Ecole.objects.all()
        nom = request.GET.get('nom', None)
        if nom is not None:
            ecole = Ecole.filter(nom__icontains=nom)
            
        Ecole_Serializer = EcoleSerializer(ecole, many=True)
        
        return JsonResponse(Ecole_Serializer.data, safe=False)

    elif request.method == 'POST':
        ecole_data = JSONParser().parse(request)
        Ecole_Serializer = EcoleSerializer(data=ecole_data)
        if Ecole_Serializer.is_valid():
            Ecole_Serializer.save()
            return JsonResponse(Ecole_Serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(Ecole_Serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Ecole.objects.all().delete()
        return JsonResponse({'message': '{} domaine were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)




"""Examen List"""
@api_view(['GET', 'POST', 'DELETE'])
def Examen_list(request):
    if request.method == 'GET':
        examen = Examen.objects.all()
        type_exam = request.GET.get('type_exam', None)
        if type_exam is not None:
            examen = Examen.filter(type_exam__icontains=type_exam)
            
        Examen_Serializer = ExamenSerializer(examen, many=True)
        
        return JsonResponse(Examen_Serializer.data, safe=False)

    elif request.method == 'POST':
        examen_data = JSONParser().parse(request)
        Examen_Serializer = ExamenSerializer(data=examen_data)
        if Examen_Serializer.is_valid():
            Examen_Serializer.save()
            return JsonResponse(Examen_Serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(Examen_Serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Examen.objects.all().delete()
        return JsonResponse({'message': '{} domaine were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)






"""Niveau_etude List"""
@api_view(['GET', 'POST', 'DELETE'])
def Niveau_etude_list(request):
    if request.method == 'GET':
        niveau_etude = Niveau_etude.objects.all()
        nom = request.GET.get('nom', None)
        if nom is not None:
            niveau_etude = Niveau_etude.filter(nom__icontains=nom)
            
        etude_serializer = Niveau_etudeSerializer(niveau_etude, many=True)
        return JsonResponse(etude_serializer.data, safe=False)

    elif request.method == 'POST':
        etude_data = JSONParser().parse(request)
        etude_serializer = Niveau_etudeSerializer(data=etude_data)
        if etude_serializer.is_valid():
            etude_serializer.save()
            return JsonResponse(etude_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(etude_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        count = Niveau_etude.objects.all().delete()
        return JsonResponse({'message': '{} Tutorials were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)






"""Taux_brut_admission list"""

@api_view(['GET', 'POST', 'DELETE'])
def admission_list(request):
	if request.method == 'GET':
		admission = Taux_brut_admission.objects.all()
		region = request.GET.get('region', None)
		if region is not None:
			admission = Taux_brut_admission.filter(region__icontains=region)
			
		admissionSerializer = Taux_brut_admissionSerializer(admission, many=True)
		return JsonResponse(admissionSerializer.data, safe=False)

	elif request.method == 'POST':
		admission_data = JSONParser().parse(request)
		admissionSerializer = Taux_brut_admissionSerializer(data=admission_data)
		if admissionSerializer.is_valid():
			admissionSerializer.save()
			return JsonResponse(Taux_brut_admissionSerializer.data, status=status.HTTP_201_CREATED) 
		return JsonResponse(admissionSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		count = Taux_brut_admission.objects.all().delete()
		return JsonResponse({'message': '{} Tutorials were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)



"""Taux_brut_scolarisation list"""

@api_view(['GET', 'POST', 'DELETE'])
def scolarisation_list(request):
	if request.method == 'GET':
		scolarisation = Taux_brut_scolarisation.objects.all()
		region = request.GET.get('region', None)
		if region is not None:
			scolarisation = Taux_brut_scolarisation.filter(region__icontains=region)
			
		scolarisationSerializer = Taux_brut_scolarisationSerializer(scolarisation, many=True)
		return JsonResponse(scolarisationSerializer.data, safe=False)

	elif request.method == 'POST':
		admission_data = JSONParser().parse(request)
		scolarisationSerializer = Taux_brut_scolarisationSerializer(data=admission_data)
		if scolarisationSerializer.is_valid():
			scolarisationSerializer.save()
			return JsonResponse(scolarisationSerializer.data, status=status.HTTP_201_CREATED) 
		return JsonResponse(scolarisationSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		count = Taux_brut_scolarisation.objects.all().delete()
		return JsonResponse({'message': '{} Tutorials were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)







"""detail"""


















"""Universite detail"""
@api_view(['GET', 'PUT', 'DELETE'])
def Universite_detail(request, nom):
    # find Universite pk (id)
    try: 
        universite = Universite.objects.get(nom=nom) 
    except Universite.DoesNotExist: 
        return JsonResponse({'message': 'The Universite does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET': 
        Universite_serializer = UniversiteSerializer(universite) 
        return JsonResponse(Universite_serializer.data) 

    elif request.method == 'PUT': 
        data = JSONParser().parse(request) 
        Universite_serializer = UniversiteSerializer(universite, data=data) 
        if Universite_serializer.is_valid(): 
            Universite_serializer.save() 
            return JsonResponse(Universite_serializer.data) 
        return JsonResponse(Universite_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE': 
        Universite.delete() 
        return JsonResponse({'message': 'Universite was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)
 


"""Formation detail"""
@api_view(['GET', 'PUT', 'DELETE'])
def Formation_detail(request, nom):
    # find Formation nom (id)
    try: 
        formation = Formation.objects.get(nom=nom) 
    except formation.DoesNotExist: 
        return JsonResponse({'message': 'The Formation does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET': 
        Formation_serializer = FormationSerializer(formation) 
        return JsonResponse(Formation_serializer.data) 

    elif request.method == 'PUT': 
        data = JSONParser().parse(request) 
        Formation_serializer = FormationSerializer(formation, data=data) 
        if Formation_serializer.is_valid(): 
            Formation_serializer.save() 
            return JsonResponse(Formation_serializer.data) 
        return JsonResponse(Formation_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE': 
        Formation.delete() 
        return JsonResponse({'message': 'Formation was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)
 



"""Domaine_formation detail"""
@api_view(['GET', 'PUT', 'DELETE'])
def Domaine_formation_detail(request, nom):
    # find Domaine_formation nom (id)
    try: 
        domaine_formation = Domaine_formation.objects.get(nom=nom) 
    except Domaine_formation.DoesNotExist: 
        return JsonResponse({'message': 'The Domaine_formation does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET': 
        Domaine_formation_serializer = Domaine_formationSerializer(domaine_formation) 
        return JsonResponse(Domaine_formation_serializer.data) 

    elif request.method == 'PUT': 
        data = JSONParser().parse(request) 
        Domaine_formation_serializer = Domaine_formationSerializer(domaine_formation, data=data) 
        if Domaine_formation_serializer.is_valid(): 
            Domaine_formation_serializer.save() 
            return JsonResponse(Domaine_formation_serializer.data) 
        return JsonResponse(Domaine_formation_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE': 
        Domaine_formation.delete() 
        return JsonResponse({'message': 'domaine_formation was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)
 



"""Ecole detail"""
@api_view(['GET', 'PUT', 'DELETE'])
def Ecole_detail(request, nom):
    # find Ecole nom (id)
    try: 
        ecole = Ecole.objects.get(nom=nom) 
    except Ecole.DoesNotExist: 
        return JsonResponse({'message': 'The Ecole does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET': 
        Ecole_serializer = EcoleSerializer(ecole) 
        return JsonResponse(Ecole_serializer.data) 

    elif request.method == 'PUT': 
        data = JSONParser().parse(request) 
        Ecole_serializer = EcoleSerializer(ecole, data=data) 
        if Ecole_serializer.is_valid(): 
            Ecole_serializer.save() 
            return JsonResponse(Ecole_serializer.data) 
        return JsonResponse(Ecole_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE': 
        Ecole.delete() 
        return JsonResponse({'message': 'Ecole was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)
 



 ###########################################################


"""Examen detail"""
@api_view(['GET', 'PUT', 'DELETE'])
def Examen_detail(request, type_exam):
    # find Examen type_exam (id)
    try: 
        examen = Examen.objects.get(type_exam=type_exam) 
    except Examen.DoesNotExist: 
        return JsonResponse({'message': 'The Examen does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET': 
        Examen_serializer = ExamenSerializer(examen) 
        return JsonResponse(Examen_serializer.data) 

    elif request.method == 'PUT': 
        data = JSONParser().parse(request) 
        Examen_serializer = ExamenSerializer(ecole, data=data) 
        if Examen_serializer.is_valid(): 
            Examen_serializer.save() 
            return JsonResponse(Examen_serializer.data) 
        return JsonResponse(Examen_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE': 
        Examen.delete() 
        return JsonResponse({'message': 'Examen was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)
 


"""Niveau_etude detail"""
@api_view(['GET', 'PUT', 'DELETE'])
def Niveau_etude_detail(request, pk):
    # find niveau_etude pk (id)
    try: 
        niveau_etude = Niveau_etude.objects.get(pk=pk) 
    except Niveau_etude.DoesNotExist: 
        return JsonResponse({'message': 'The Niveau_etude does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET': 
        Niveau_etude_serializer = Niveau_etudeSerializer(niveau_etude) 
        return JsonResponse(Niveau_etude_serializer.data) 

    elif request.method == 'PUT': 
        Niveau_etude_data = JSONParser().parse(request) 
        Niveau_etude_serializer = Niveau_etudeSerializer(niveau_etude, data=Niveau_etude_data) 
        if Niveau_etude_serializer.is_valid(): 
            Niveau_etude_serializer.save() 
            return JsonResponse(Niveau_etude_serializer.data) 
        return JsonResponse(Niveau_etude_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE': 
        Niveau_etude.delete() 
        return JsonResponse({'message': 'Niveau_etude was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)
 
    # GET / PUT / DELETE tutorial



"""Taux_brut_admission detail"""
@api_view(['GET', 'PUT', 'DELETE'])
def Taux_brut_admission_detail(request, region):
    # find Taux_brut_admission region (id)
    try: 
        admission = Taux_brut_admission.objects.get(region=region) 
    except Taux_brut_admission.DoesNotExist: 
        return JsonResponse({'message': 'The Taux_brut_admission does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET': 
        Taux_brut_admission_serializer = Taux_brut_admissionSerializer(admission) 
        return JsonResponse(Taux_brut_admission_serializer.data) 

    elif request.method == 'PUT': 
        Taux_brut_admission_data = JSONParser().parse(request) 
        Taux_brut_admission_serializer = Taux_brut_admissionSerializer(admission, data=Taux_brut_admission_data) 
        if Taux_brut_admission_serializer.is_valid(): 
            Taux_brut_admission_serializer.save() 
            return JsonResponse(Taux_brut_admission_serializer.data) 
        return JsonResponse(Taux_brut_admission_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE': 
        Taux_brut_admission.delete() 
        return JsonResponse({'message': 'Taux_brut_admission was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)
 
    # GET / PUT / DELETE tutorial



    """Taux_brut_scolarisation detail"""
@api_view(['GET', 'PUT', 'DELETE'])
def Taux_brut_scolarisation_detail(request, region):
    # find Taux_brut_scolarisation region (id)
    try: 
        scolarisation = Taux_brut_scolarisation.objects.get(region=region) 
    except Taux_brut_scolarisation.DoesNotExist: 
        return JsonResponse({'message': 'The Taux_brut_scolarisation does not exist'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET': 
        Taux_brut_scolarisation_serializer = Taux_brut_scolarisationSerializer(scolarisation) 
        return JsonResponse(Taux_brut_scolarisation_serializer.data) 

    elif request.method == 'PUT': 
        Taux_brut_scolarisation_data = JSONParser().parse(request) 
        Taux_brut_scolarisation_serializer = Taux_brut_scolarisationSerializer(scolarisation, data=Taux_brut_scolarisation_data) 
        if Taux_brut_scolarisation_serializer.is_valid(): 
            Taux_brut_scolarisation_serializer.save() 
            return JsonResponse(Taux_brut_scolarisation_serializer.data) 
        return JsonResponse(Taux_brut_scolarisation_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE': 
        Taux_brut_scolarisation.delete() 
        return JsonResponse({'message': 'Taux_brut_scolarisation was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)
 
    # GET / PUT / DELETE tutorial



"""////////////////////////////"""

# @api_view(['GET'])
# def tutorial_list_published(request):
#     # GET all published tutorials

#     tutorials = Tutorial.objects.filter(published=True)
        
#     if request.method == 'GET': 
#         tutorials_serializer = TutorialSerializer(tutorials, many=True)
#         return JsonResponse(tutorials_serializer.data, safe=False)
