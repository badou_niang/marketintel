from django.contrib import admin
from .models import *
from import_export.admin import ImportExportModelAdmin
from import_export import resources
# Register your models here.

@admin.register(Niveau_etude)
class niveau_etudeResource(ImportExportModelAdmin):
	pass

@admin.register(Taux_brut_admission)
class Taux_brut_admissionResource(ImportExportModelAdmin):
	pass

@admin.register(Taux_brut_scolarisation)
class Taux_brut_scolarisationResource(ImportExportModelAdmin):
	pass


@admin.register(Universite)
class UniversiteResource(ImportExportModelAdmin):
	pass

@admin.register(Formation)
class FormationResource(ImportExportModelAdmin):
	pass

@admin.register(Domaine_formation)
class Domaine_formationResource(ImportExportModelAdmin):
	pass

@admin.register(Ecole)
class EcoleResource(ImportExportModelAdmin):
	pass

@admin.register(Examen)
class ExamenResource(ImportExportModelAdmin):
	pass

	
@admin.register(depense_etat)
class depense_etatResource(ImportExportModelAdmin):
	pass