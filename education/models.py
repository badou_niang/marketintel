from django.db import models
import datetime

# Create your models here.

class Niveau_etude(models.Model):

    nom = models.CharField(max_length=60, unique=True, blank=False, null=False)

    def __str__(self):
        return self.nom

# ce modele represente le taux d'admission pour tous les niveaux


class Taux_brut_admission(models.Model):

    region = models.CharField(max_length=30)
    garcon = models.IntegerField(default=0)
    fille = models.IntegerField(default=0)
    total = models.IntegerField(default=0)
    annee = models.IntegerField()
    commentaire = models.TextField(null=True, blank=True)
#  ce champ fait la jointure avec le type de niveau auquel les données appartienent
    niveau_etude = models.ForeignKey(Niveau_etude, on_delete=models.CASCADE)

    def __str__(self):
        return self.region

# Ce modele represente le taux brut de prescolarisation


class Taux_brut_scolarisation(models.Model):

    region = models.CharField(max_length=30)
    garcon = models.FloatField(default=0)
    fille = models.FloatField(default=0)
    total = models.FloatField(default=0)
    indice_de_parite = models.FloatField(default=0)
    commentaire = models.TextField(blank=True, null=True)
    annee = models.IntegerField(default=datetime.datetime.now().year)
    niveau_etude = models.ForeignKey(Niveau_etude, on_delete=models.CASCADE)

    def __str__(self):
        return self.region

#######################################################################

class Ecole (models.Model):
    nom = models.CharField(max_length=100, blank=False, default='')
    region = models.CharField(max_length=20, blank=False, default='')
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.nom


class Universite(models.Model):
    nom = models.CharField(max_length=100, blank=False, default='')
    type_univ = models.CharField(max_length=70, blank=False, default='')
    # description = models.TextField(blank=True, null=True)
    # nbr_etudiant = models.IntegerField(default=0)
    # emplacement = models.CharField(max_length=100, blank=False, default='')
    # appreciation = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.nom


class Domaine_formation(models.Model):
    nom = models.CharField(max_length=70, blank=False, default='')

    def __str__(self):
        return self.nom


class Formation(models.Model):
    nom = models.CharField(max_length=100, blank=False, default='')
    domaine = models.ForeignKey(Domaine_formation, on_delete=models.CASCADE)
    ecole = models.ManyToManyField(Ecole, related_name='formation', blank=True)
    universite = models.ManyToManyField(Universite, related_name='formation', blank=True)

    def __str__(self):
        return self.nom



class Examen(models.Model):
    annee = models.IntegerField(default=0)
    type_exam = models.CharField(max_length=10, blank=False, default='')
    nbr_candidat = models.IntegerField(default=0) 
    nbr_present = models.IntegerField(default=0)   
    nbr_session_normale = models.IntegerField(default=0)   
    taux_admission = models.FloatField(default=0)  
    candidat_au_second_tour = models.IntegerField(default=0) 
    mention_tres_bien = models.IntegerField(default=0)   
    mention_bien = models.IntegerField(default=0)    
    mention_Assez_bien = models.IntegerField(default=0)  
    mention_Passable = models.IntegerField(default=0)

    def __str__(self):
        return self.type_exam


class depense_etat(models.Model):
    annee = models.IntegerField(default=datetime.datetime.now().year)
    depense_pour_education_en_m = models.FloatField(default=0)
    depense_en_education_pourcent_budgetaire = models.FloatField(default=0) 
    depense_en_education_pourcent_pib = models.FloatField(default=0)   
    

    def __str__(self):
        annee = str(self.annee)
        return annee