from django.conf import settings
from django.db import models


"""création de la table restaurant pour stocker les données scrappées"""
class Restaurant(models.Model):  
    nom = models.CharField(max_length=100, blank=True, default='')
    adresse=models.CharField(max_length=100, blank=True, default='')
    description = models.TextField(blank=True)
    telephone=models.CharField(max_length=100, blank=True, default='')
    pays=models.CharField(max_length=100, blank=True, default='')
    region=models.CharField(max_length=100, blank=True, default='')

    def __str__(self):
        return '<Restaurants: {}>'.format(self.nom)