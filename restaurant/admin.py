from django.contrib import admin
from .models import Restaurant
from import_export.admin import ImportExportModelAdmin
from import_export import resources
# Register your models here.

@admin.register(Restaurant)
class RestaurantResource(ImportExportModelAdmin):
	pass
