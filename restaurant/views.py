# from django.shortcuts import render

# Create your views here.

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
from django.http import HttpResponse
 

from restaurant.models import Restaurant
from restaurant.serializers import RestaurantSerializer
from rest_framework.decorators import api_view



""" 
GET list of restaurant, POST a new restaurant, DELETE all restaurant
"""
"""restaurant List"""
@api_view(['GET', 'POST', 'DELETE'])
def Restaurantlist(request):
	if request.method == 'GET':
		resto = Restaurant.objects.all()
		nom = request.GET.get('nom', None)
		if nom is not None:
			resto = resto.filter(nom__icontains=nom)
			
		restaurantSerializer = RestaurantSerializer(resto, many=True)
		return JsonResponse(restaurantSerializer.data, safe=False)

	elif request.method == 'POST':
		data = JSONParser().parse(request)
		restaurantSerializer = RestaurantSerializer(data=data)
		if restaurantSerializer.is_valid():
			restaurantSerializer.save()
			return JsonResponse(restaurantSerializer.data, status=status.HTTP_201_CREATED) 
		return JsonResponse(restaurantSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		count = Restaurant.objects.all().delete()
		return JsonResponse({'message': '{} Restaurant were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)



"""detail"""


"""Cars detail"""
@api_view(['GET', 'PUT', 'DELETE'])
def Restaurantdetail(request, pk):
    # find cars by pk (id)
    try: 
        resto = Restaurant.objects.get(pk=pk) 
    except resto.DoesNotExist: 
        return JsonResponse({'message': 'This resto does not exist in the database'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET': 
        restaurantSerializer = RestaurantSerializer(resto) 
        return JsonResponse(restaurantSerializer.data) 

    elif request.method == 'PUT': 
        data = JSONParser().parse(request) 
        restaurantSerializer = RestaurantSerializer(resto, data=data) 
        if restaurantSerializer.is_valid(): 
            restaurantSerializer.save() 
            return JsonResponse(restaurantSerializer.data) 
        return JsonResponse(restaurantSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE': 
        resto.delete() 
        return JsonResponse({'message': 'resto was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)
 
