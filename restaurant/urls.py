from django.urls import path
from . import views

urlpatterns = [
    path('resto', views.Restaurantlist),
    path('resto/<int:pk>', views.Restaurantdetail),
]
