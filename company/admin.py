from django.contrib import admin
from .models import Entreprise
from import_export.admin import ImportExportModelAdmin
from import_export import resources
# Register your models here.

@admin.register(Entreprise)
class EntrepriseResource(ImportExportModelAdmin):
	pass
