from django.db import models

#Creation de modele pour faire le lien avec la base de donnée
class Entreprise(models.Model):
    nom = models.CharField(max_length=100, blank=True, default='')
    adresse = models.CharField(max_length=100, blank=True, default='')
    telephone = models.CharField(max_length=100, blank=True, default='')
    pays = models.CharField(max_length=100, blank=True, default='')
    region = models.CharField(max_length=100, blank=True, default='')
    secteur = models.CharField(max_length=100, blank=True, default='')
