from django.urls import path
from . import views

urlpatterns = [
    path('company', views.Companylist),
    path('company/<int:pk>', views.Companydetail),
]
