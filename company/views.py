# from django.shortcuts import render

# Create your views here.

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 

from company.models import Entreprise
from company.serializers import EntrepriseSerializer
from rest_framework.decorators import api_view



""" 
GET list of company, POST a new Company, DELETE all Company
"""
"""Company List"""
@api_view(['GET', 'POST', 'DELETE'])
def Companylist(request):
	if request.method == 'GET':
		company = Entreprise.objects.all()
		nom = request.GET.get('nom', None)
		if nom is not None:
			company = company.filter(nom__icontains=nom)
			
		entrepriseSerializer = EntrepriseSerializer(company, many=True)
		return JsonResponse(entrepriseSerializer.data, safe=False)

	elif request.method == 'POST':
		data = JSONParser().parse(request)
		entrepriseSerializer = EntrepriseSerializer(data=data)
		if entrepriseSerializer.is_valid():
			entrepriseSerializer.save()
			return JsonResponse(entrepriseSerializer.data, status=status.HTTP_201_CREATED) 
		return JsonResponse(entrepriseSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		count = Entreprise.objects.all().delete()
		return JsonResponse({'message': '{} Company were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)



"""detail"""


"""Cars detail"""
@api_view(['GET', 'PUT', 'DELETE'])
def Companydetail(request, pk):
    # find cars by pk (id)
    try: 
        company = Entreprise.objects.get(pk=pk) 
    except company.DoesNotExist: 
        return JsonResponse({'message': 'This Company does not exist in the database'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET': 
        entrepriseSerializer = EntrepriseSerializer(company) 
        return JsonResponse(entrepriseSerializer.data) 

    elif request.method == 'PUT': 
        data = JSONParser().parse(request) 
        entrepriseSerializer = EntrepriseSerializer(company, data=data) 
        if entrepriseSerializer.is_valid(): 
            entrepriseSerializer.save() 
            return JsonResponse(entrepriseSerializer.data) 
        return JsonResponse(entrepriseSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE': 
        company.delete() 
        return JsonResponse({'message': 'Company was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)
 
