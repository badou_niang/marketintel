from django.contrib import admin
from .models import Offre
from import_export.admin import ImportExportModelAdmin
from import_export import resources
# Register your models here.

@admin.register(Offre)
class OffreResource(ImportExportModelAdmin):
	pass
