from django.db import models

class Offre(models.Model):
    intitule = models.CharField(max_length=100, blank=True, default='')
    description = models.TextField(blank=True, null=True)
    secteur = models.CharField(max_length=100, blank=True, default='')
    salaire = models.CharField(max_length=100, blank=True, default='')
    typeContrat = models.CharField(max_length=100, blank=True, default='')
    experience = models.CharField(max_length=100, blank=True, default='')
    niveau = models.CharField(max_length=100, blank=True, default='')
    entreprise = models.CharField(max_length=100, blank=True, default='')
    date = models.CharField(max_length=100, blank=True, default='')
    lieu = models.CharField(max_length=100, blank=True, default='')
    pays = models.CharField(max_length=100, blank=True, default='')
    lien = models.CharField(max_length=255, default=' ')
