from django.shortcuts import render

# Create your views here.

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 

from emploi.models import Offre
from emploi.serializers import OffreSerializer
from rest_framework.decorators import api_view



""" 
GET list of offre, POST a new offre, DELETE all offre
"""
"""Emploi List"""
@api_view(['GET', 'POST', 'DELETE'])
def Offrelist(request):
	if request.method == 'GET':
		offre = Offre.objects.all()
		intitule = request.GET.get('intitule', None)
		if intitule is not None:
			offre = offre.filter(intitule__icontains=intitule)
			
		offreSerializer = OffreSerializer(offre, many=True)
		return JsonResponse(offreSerializer.data, safe=False)

	elif request.method == 'POST':
		data = JSONParser().parse(request)
		offreSerializer = OffreSerializer(data=data)
		if offreSerializer.is_valid():
			offreSerializer.save()
			return JsonResponse(offreSerializer.data, status=status.HTTP_201_CREATED) 
		return JsonResponse(offreSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		count = Offre.objects.all().delete()
		return JsonResponse({'message': '{} Offre were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)



"""detail"""


"""Cars detail"""
@api_view(['GET', 'PUT', 'DELETE'])
def Offredetail(request, pk):
    # find cars by pk (id)
    try: 
        offre = Offre.objects.get(pk=pk) 
    except offre.DoesNotExist: 
        return JsonResponse({'message': 'This offre does not exist in the database'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET': 
        offreSerializer = OffreSerializer(offre) 
        return JsonResponse(offreSerializer.data) 

    elif request.method == 'PUT': 
        data = JSONParser().parse(request) 
        offreSerializer = OffreSerializer(offre, data=data) 
        if offreSerializer.is_valid(): 
            offreSerializer.save() 
            return JsonResponse(offreSerializer.data) 
        return JsonResponse(offreSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE': 
        offre.delete() 
        return JsonResponse({'message': 'offre was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)


