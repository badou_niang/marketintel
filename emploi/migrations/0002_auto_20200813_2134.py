# Generated by Django 2.1.7 on 2020-08-13 21:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('emploi', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='offre',
            name='id',
            field=models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='offre',
            name='lien',
            field=models.CharField(default=' ', max_length=255),
        ),
    ]
