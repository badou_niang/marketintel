from rest_framework import serializers 
from emploi.models import Offre
 
 
class OffreSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Offre
        fields = '__all__'