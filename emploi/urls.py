from django.urls import path
from . import views

urlpatterns = [
    path('offre', views.Offrelist),
    path('offre/<int:pk>', views.Offredetail),
]
