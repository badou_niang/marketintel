from django.contrib import admin
from .models import Immobilier
from import_export.admin import ImportExportModelAdmin
from import_export import resources
# Register your models here.

@admin.register(Immobilier)
class ImmobilierResource(ImportExportModelAdmin):
	pass
