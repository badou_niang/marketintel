from django.urls import path
from . import views

urlpatterns = [
    path('immo', views.Immolist),
    path('immo/<int:pk>', views.Immodetail),
]
