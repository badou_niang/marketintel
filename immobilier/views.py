from django.shortcuts import render

# Create your views here.

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 

from immobilier.models import Immobilier
from immobilier.serializers import ImmobilierSerializer
from rest_framework.decorators import api_view



""" 
GET list of offre, POST a new offre, DELETE all offre
"""
"""Emploi List"""
@api_view(['GET', 'POST', 'DELETE'])
def Immolist(request):
	if request.method == 'GET':
		immo = Immobilier.objects.all()
		intitule = request.GET.get('intitule', None)
		if intitule is not None:
			immo = immo.filter(intitule__icontains=intitule)
			
		immoSerializer = ImmobilierSerializer(immo, many=True)
		return JsonResponse(immoSerializer.data, safe=False)

	elif request.method == 'POST':
		data = JSONParser().parse(request)
		immoSerializer = ImmobilierSerializer(data=data)
		if immoSerializer.is_valid():
			immoSerializer.save()
			return JsonResponse(immoSerializer.data, status=status.HTTP_201_CREATED) 
		return JsonResponse(immoSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		count = Immobilier.objects.all().delete()
		return JsonResponse({'message': '{} Immobilie were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)



"""detail"""


"""Cars detail"""
@api_view(['GET', 'PUT', 'DELETE'])
def Immodetail(request, pk):
    # find cars by pk (id)
    try: 
        immo = Immobilier.objects.get(pk=pk) 
    except immo.DoesNotExist: 
        return JsonResponse({'message': 'This Immobilier does not exist in the database'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET': 
        immoSerializer = ImmobilierSerializer(immo) 
        return JsonResponse(immoSerializer.data) 

    elif request.method == 'PUT': 
        data = JSONParser().parse(request) 
        immoSerializer = ImmobilierSerializer(immo, data=data) 
        if immoSerializer.is_valid(): 
            immoSerializer.save() 
            return JsonResponse(immoSerializer.data) 
        return JsonResponse(immoSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE': 
        immo.delete() 
        return JsonResponse({'message': 'Immobilier was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)


