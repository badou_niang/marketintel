from django.db import models


"""
    Créons le model qui nous permettra de charger les offres immobilières de notre BDD
"""
class Immobilier(models.Model):
    type_immo = models.CharField(max_length=45, blank=True, null=True)
    city = models.CharField(max_length=45, blank=True, null=True)
    country = models.CharField(max_length=45, blank=True, null=True)
    intitule = models.CharField(max_length=100, blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    monnaie = models.CharField(max_length=45, blank=True, null=True)
    seller = models.CharField(max_length=45, blank=True, null=True)
    date = models.CharField(max_length=45, blank=True, null=True)
    surface = models.IntegerField(blank=True, null=True)
    chambre = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.intitule

