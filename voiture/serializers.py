from rest_framework import serializers 
from voiture.models import Cars
 
 
class CarsSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Cars
        fields = '__all__'