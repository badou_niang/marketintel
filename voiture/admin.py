from django.contrib import admin
from .models import Cars
from import_export.admin import ImportExportModelAdmin
from import_export import resources
# Register your models here.

@admin.register(Cars)
class carsResource(ImportExportModelAdmin):
	pass
