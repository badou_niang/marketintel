from django.urls import path
from . import views

urlpatterns = [
    path('cars', views.Carslist),
    path('cars/<int:pk>', views.Carsdetail),
]
