# from django.shortcuts import render

# Create your views here.

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 

from voiture.models import Cars
from voiture.serializers import CarsSerializer
from rest_framework.decorators import api_view



""" 
GET list of Cars, POST a new Car, DELETE all car
"""
"""Car List"""
@api_view(['GET', 'POST', 'DELETE'])
def Carslist(request):
	if request.method == 'GET':
		car = Cars.objects.all()
		modele = request.GET.get('modele', None)
		if modele is not None:
			car = car.filter(modele__icontains=modele)
			
		carsSerializer = CarsSerializer(car, many=True)
		return JsonResponse(carsSerializer.data, safe=False)

	elif request.method == 'POST':
		car_data = JSONParser().parse(request)
		carsSerializer = CarsSerializer(data=car_data)
		if carsSerializer.is_valid():
			carsSerializer.save()
			return JsonResponse(carsSerializer.data, status=status.HTTP_201_CREATED) 
		return JsonResponse(carsSerializer.errors, status=status.HTTP_400_BAD_REQUEST)

	elif request.method == 'DELETE':
		count = Cars.objects.all().delete()
		return JsonResponse({'message': '{} cars were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)



"""detail"""


"""Cars detail"""
@api_view(['GET', 'PUT', 'DELETE'])
def Carsdetail(request, pk):
    # find cars by pk (id)
    try: 
        cars = Cars.objects.get(pk=pk) 
    except cars.DoesNotExist: 
        return JsonResponse({'message': 'This cars does not exist in the database'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET': 
        carsserializer = CarsSerializer(cars) 
        return JsonResponse(carsserializer.data) 

    elif request.method == 'PUT': 
        data = JSONParser().parse(request) 
        carsserializer = CarsSerializer(cars, data=data) 
        if carsserializer.is_valid(): 
            carsserializer.save() 
            return JsonResponse(carsserializer.data) 
        return JsonResponse(carsserializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE': 
        cars.delete() 
        return JsonResponse({'message': 'cars was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)
 
