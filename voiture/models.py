from django.db import models

# Create your models here.


class Cars(models.Model):
    constructeur = models.CharField(max_length=100, blank=False, default='')
    modele = models.CharField(max_length=100, blank=True, default='')
    kilometrage = models.IntegerField(default=0)
    carburant = models.CharField(max_length=100, blank=True, default='')  
    annee = models.CharField(max_length=100, blank=True, default='')
    localisation = models.CharField(max_length=100, blank=True, default='')
    contact = models.CharField(max_length=100, blank=True, default='')
    transmission = models.CharField(max_length=100, blank=True, default='')
    prix = models.FloatField(default=0)
    vendeur = models.CharField(max_length=100, blank=True, default='')

